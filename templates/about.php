<?php
/**
 * Template Name: About
 */

 get_header(); ?>

<div class="main-wrapper">
    <div class="row">
      <div class="col-md-11">
        <!--Page Content-->
        <div class="content">
          <div class="row m-t-1">
             <div class="col-md-4">
                <h3><?php the_field('red_section_', $post->ID); ?></h3>
                <div class="m-t-3">
                  <p class="red-p">
                    <?php the_field('red_section_p', $post->ID); ?>
                  </p>
                </div>
                 <div class="m-t-3">
                     <h6>Team:</h6>
                     <p>
                      <?php the_field('team', $post->ID); ?>
                     </p>
                 </div>
                    
                    
                  <div class="m-t-3">
                    <h6>What:</h6>
                    <p><?php the_field('what', $post->ID); ?></p>
                  </div>
                    

                    <div class="m-t-3">
                      <h6>Team:</h6>
                      <p><?php the_field('mision', $post->ID); ?></p>
                    </div>
             </div>
             <div class="col-md-8">
                <img src="<?php the_post_thumbnail_url(); ?>" class="about-img" alt="">
             </div>
          </div>
        </div>
      </div>
      <div class="col-md-1">
        <!-- Right Sidebar -->
        <?php get_template_part('template-parts/home/content', 'sidebar'); ?>
      </div>

    </div>
  </div>

    <div class="collapsed-tabs">
        <?php get_template_part('template-parts/home/content', 'tabs'); ?>
    </div>


<?php get_footer(); ?>