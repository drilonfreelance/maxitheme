<?php
/**
 * Template Name: Template-Home
 */
get_header(); 

$slug = 'template-parts/home/content';

?>

<div class="main-wrapper">
        <div class="row">
            <div class="col-md-11">
                <?php get_template_part($slug, 'categories'); ?>
                <?php get_template_part($slug, 'products'); ?>
                <!--Page Content-->
                <?php get_template_part($slug, 'banner'); ?>
                <?php get_template_part($slug, 'about'); ?>
                <!-- End Page Content -->
            </div>
            <div class="col-md-1">
                <!-- Right Sidebar -->
                <?php get_template_part($slug, 'sidebar'); ?>
            </div>
        </div>
    </div>
    <?php get_template_part($slug, 'tabs'); ?>


<?php get_footer(); ?>