<?php

//Autoloader
require_once 'theme-autoloader.php';
Theme_autoloader::register();

require 'ajax.php';

//Widgets
Customwidget::register( 'Products Search', 'product_search' );

//Theme support
Themesupport::add_active_nav();
Themesupport::add_theme_support( 'post-thumbnails' );
Themesupport::add_theme_support( 'custom-logo' );
Themesupport::add_theme_support( 'title-tag' );
Themesupport::add_theme_support( 'woocommerce' );
Themesupport::register_nav_menu( 'main_menu' ,'Navigation Menu' );

// Script functions
Script::init_built_in_libraries();

//Admin functions
// AdminScript::hide_menu('plugins.php');
AdminScript::load_admin_style();