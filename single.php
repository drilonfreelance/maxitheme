<?php get_header(); ?>
<div class="clearfix"></div>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			
            the_content();
            the_title();

		endwhile;
		wp_reset_postdata();
		?>

		</main><!-- .site-main -->
	</div><!-- .content-area -->
    <div class="clearfix"></div>
<?php get_footer(); ?>