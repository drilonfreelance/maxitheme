<?php
defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_lost_password_form' );
?>
<div class="custom-reset-password">
	<style>
		.woocommerce-error{
			position: absolute;
			top:0;
			left:0;
			right:0;
			z-index:999;
		}
		.custom-reset-password{
			position:fixed;
			top:0;
			right:0;
			left:0;
			bottom:0;
			background-color:#e60019;
			z-index:998;
		}
		.custom-reset-password .top-logo{
			padding: 50px;
			text-align: center;
			margin: auto;
		}
		form{
			width: fit-content;
			margin: 0 auto;
			padding: 100px;
		}
		form p{
			color:#fff;
			max-width:400px;
		}
	</style>
<div class="top-logo">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/logo-maxi2.png" alt="">
</div>
<form method="post" class="woocommerce-ResetPassword lost_reset_password">

	<p><?php echo apply_filters( 'woocommerce_lost_password_message', esc_html__( 'Lost your password? Please enter your username or email address. You will receive a link to create a new password via email.', 'woocommerce' ) ); ?></p><?php // @codingStandardsIgnoreLine ?>

	<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
		<label for="user_login"><?php esc_html_e( 'Username or email', 'woocommerce' ); ?></label>
		<input class="woocommerce-Input woocommerce-Input--text input-text" type="text" name="user_login" id="user_login" autocomplete="username" />
	</p>

	<div class="clear"></div>

	<?php do_action( 'woocommerce_lostpassword_form' ); ?>

	<p class="woocommerce-form-row form-row">
		<input type="hidden" name="wc_reset_password" value="true" />
		<button type="submit" class="woocommerce-Button button" value="<?php esc_attr_e( 'Reset password', 'woocommerce' ); ?>"><?php esc_html_e( 'Reset password', 'woocommerce' ); ?></button>
	</p>

	<?php wp_nonce_field( 'lost_password', 'woocommerce-lost-password-nonce' ); ?>

</form>
</div>
<?php
do_action( 'woocommerce_after_lost_password_form' );
