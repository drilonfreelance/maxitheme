<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_account_navigation' );
?>

<div class="right-sidebar">
  <ul class="menu">
    <li class="parent" data-id="shopping-account">
      <a href="#"><span class="account-span">Kycu</span></a>
    </li>
    <li class="parent" data-id="profile-info">
      <a href="#"
        ><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/menu/ikona-menu-account.svg" alt=""
      /></a>
    </li>
    <li class="parent" data-id="profile-order">
      <a href="#" class="menu-cart"
        ><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/menu/ikona-menu-shporta.svg" alt=""
      /></a>
    </li>
    <li class="parent" data-id="favourites-products">
      <a href="#"
        ><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/menu/ikona-menu-favourites.svg" alt=""
      /></a>
    </li>
    <li class="parent" data-id="waiting-order">
      <a href="#"
        ><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/menu/ikona-menu-historia.svg" alt=""
      /></a>
    </li>
    <li class="parent" data-id="settings-info">
      <a href="#"
        ><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/menu/ikona-menu-settings.svg" alt=""
      /></a>
    </li>
    <li class="parent bottom-menu" data-id="message-holder">
      <a href="#"
        ><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/menu/ikona-menu-chat.svg" alt=""
      /></a>
    </li>
  </ul>
</div>





<nav class="woocommerce-MyAccount-navigation">
	<ul>
		<?php foreach ( wc_get_account_menu_items() as $endpoint => $label ) : ?>
			<li class="<?php echo wc_get_account_menu_item_classes( $endpoint ); ?>">
				<a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint ) ); ?>"><?php echo esc_html( $label ); ?></a>
			</li>
		<?php endforeach; ?>
	</ul>
</nav>

<?php do_action( 'woocommerce_after_account_navigation' ); ?>
