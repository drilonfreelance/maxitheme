<?php

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div class="container">
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
		<?php
	
		 remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title' ,5);
		 remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price' ,10);
		 remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating' ,10);
		 remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt' ,20);
		 remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing' ,50);
		?>

	<div class="content-single">
                   <div class="container">
                       <div class="single-product-details">
                           <div class="row">
                               <div class="col-md-6">
                                  <div class="product-image">
									  <?php $_product_image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->get_id() ) )[0]; ?>
                                    <img src="<?php echo $_product_image; ?>" alt="">
                                  </div>
                               </div>
                               <div class="col-md-6">
                                  <div class="product-info">
                                      <h6><?php the_title(); ?></h6>
                                      <div class="price">
                                          <span class="span-price"><?php echo $product->get_price_html(); ?> </span>
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/albanian-flag.png" class="flag-img " alt="">
                                        <?php do_action( 'woocommerce_single_product_summary' ); ?>
									  </div>
									  <?php echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
                                  </div>
                               </div>
                           </div>
                       </div>
                   </div>
                </div>
            </div>

	<?php
	
	remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
	remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
	?>

	<div class="related">
		<?php do_action( 'woocommerce_after_single_product_summary' );?>
	</div>
	
</div>
<?php do_action( 'woocommerce_after_single_product' ); ?>
