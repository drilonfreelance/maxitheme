<?php

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<li <?php wc_product_class( '', $product ); ?>>
	<?php
	/**
	 * Hook: woocommerce_before_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item' );

	?>
	<div class="flag-origin">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/albanian-flag.png" alt="" />
    </div>

	<style>
		.woocommerce .products .flag-origin{
			max-width:100% !important;
			width:100% !important;
			float:left !important;
			padding: 20px 0;
		}
		.woocommerce .products .flag-origin img{
			width:20% !important;
			height:auto !important;
			margin:0 !important;
			text-align:left !important;
		}
		.add_to_wishlist svg{
			fill:#fc0b08;
			height:40px;
			margin-top:30px;
		}
		.heart-cart{
			display: flex;
			align-items: center;
			justify-content: center;
		}
	</style>
	<?php

	/**
	 * Hook: woocommerce_before_shop_loop_item_title.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	do_action( 'woocommerce_shop_loop_item_title' );
	

	/**
	 * Hook: woocommerce_after_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	
	?>
	<div class="heart-cart">
	<?php
	echo do_shortcode('[yith_wcwl_add_to_wishlist]');
	do_action( 'woocommerce_after_shop_loop_item' );
	?>
	</div>
</li>
