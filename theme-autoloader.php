<?php
class Theme_autoloader{

    public static function register($prepend = false){
        if (version_compare(phpversion(), '5.3.0', '>=')) {
            spl_autoload_register(array(new self, 'autoload'), true, $prepend);
        } else {
            spl_autoload_register(array(new self, 'autoload'));
        }
    }

    public static function autoload($class){ 
        if(is_file($file = dirname(__FILE__) . '/models/'.strtolower($class).'.php')){
            require_once $file;
        }
    }
    
}