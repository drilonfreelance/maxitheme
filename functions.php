<?php 
require 'main.php';

add_filter('woocommerce_product_add_to_cart_text', function(){
    return __('SHTO NË SHPORTË', 'woocommerce');
});   

function dswp_add_ajaxurl(){
    ?>    
        <script>var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';</script>    
    <?php
}
add_action('wp_footer', 'dswp_add_ajaxurl');

add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );
function woocommerce_header_add_to_cart_fragment( $fragments ) {
	ob_start();
	?>
    <span class="value"><?php echo WC()->cart->get_cart_total(); ?></span>	

    <?php
	
   $fragments['.vlera-shportes span.value'] = ob_get_clean();
	return $fragments;
}

function my_text_strings( $translated_text, $text, $domain ) {
    switch ( $translated_text ) {
    case 'View cart' :
    $translated_text = __( '', 'woocommerce' );
    break;
    }
    return $translated_text;
    }
    add_filter( 'gettext', 'my_text_strings', 20, 3 );
    add_filter( 'woocommerce_product_single_add_to_cart_text', 'bbloomer_custom_add_cart_button_single_product' );
 
    function bbloomer_custom_add_cart_button_single_product( $label ) {
        
       foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {
          $product = $values['data'];
          if( get_the_ID() == $product->get_id() ) {
             $label = __('E SHTUAR NË SHPORTË', 'woocommerce');
          }
       }
        
       return $label;
     
    }
     
    // Part 2
    // Edit Loop Pages Add to Cart
     
    add_filter( 'woocommerce_product_add_to_cart_text', 'bbloomer_custom_add_cart_button_loop', 99, 2 );
     
    function bbloomer_custom_add_cart_button_loop( $label, $product ) {
        
       if ( $product->get_type() == 'simple' && $product->is_purchasable() && $product->is_in_stock() ) {
           
          foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {
             $_product = $values['data'];
             if( get_the_ID() == $_product->get_id() ) {
                $label = __('E SHTUAR NË SHPORTË', 'woocommerce');
             }
          }
           
       }
        
       return $label;
        
    }
/**
 * @TODO:
 * CHANGE PERMISSION OF DOCUMENT ROOT
 * CHANGE PERMISSION OF THEME
 * CHANGE PERMISSION OF PLUGIN
 */