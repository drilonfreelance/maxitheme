<!-- Header -->
<?php $image = wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ) , 'full' )[0]; ?>
<header>
        <div class="row">
            <div class="col-md-2 left">
                <div class="logo">
                    <a href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php echo esc_url( $image ); ?>" alt="<?php echo esc_attr( the_title() ); ?>" /></a>
                </div>
            </div>
            <div class="col-md-6 center">
                <?php get_product_search_form(); ?>
            </div>
            <div class="col-md-4">
                <ul>
                    <li class="vlera-shportes">
                        VLERA MONETARE
                        <span class="value"><?php echo WC()->cart->get_cart_total(); ?></span>
                    </li>
                    <li class="social-icons"><a href="#"><i class="fa fa-facebook"></i></a> </i>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                    </li>
                    <li class="language-li">
                        <div class="language"><img src="assets/images/icons/albanian-flag.png" alt="">
                        </div>
                        <div class="language"><img src="assets/images/icons/england-flag.png" alt="">
                        </div>
                    </li>
                </ul>
            </div>
    </header>
    <!--End Header -->
