    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <ul>
                        <li><a href="#">KUSHTET E PËRDORIMIT</a></li>
                        <li><a href="#">KUSHTET E TRANSPORTIT</a></li>
                        <li><a href="#">SI TË REGJISTROHEM?</a></li>
                        <li><a href="#">SI TË POROSITI PRODUKTE?</a></li>
                    </ul>
                </div>
                <div class="col-md-5">
                    <ul class="right-list">
                        <li><span>Qendra e thirrjeve</span></li>
                        <li><i class="fa fa-phone"></i><span class="span-number">0800 100 3000</span></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="f-copyright">
            <div class="center">
                <p>ALL RIGHTS RESERVED. MAXI E-SHOP 2019.</p>
            </div>
        </div>
    </footer>
