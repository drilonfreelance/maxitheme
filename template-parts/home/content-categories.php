<?php
 $all_categories = get_categories( array(
      'taxonomy'     => 'product_cat',
      'orderby'      => 'name',
      'show_count'   => 0,
      'pad_counts'   => 0,
      'hierarchical' => 0,
      'title_li'     => '',
      'hide_empty'   => 0,
      'parent'  => 0
    )
  );
?>

<div class="categories">
  <ul class="nav nav-tabs" id="myTab" role="tablist">
    <?php
      foreach ($all_categories as $cat) {    
        $image = wp_get_attachment_url( get_term_meta( $cat->term_id, 'thumbnail_id', true ) );
        ?>
        <li class="nav-item">
            <a
              class="nav-link"
              onclick="load_sub_categories('<?php echo $cat->term_id; ?>', this.parentElement)"
            >
              <img src="<?php echo $image; ?>" alt="" /> <?php echo $cat->name; ?>
            </a>
          </li>
        <?php
      }
      ?>
  </ul>
</div>

<div class="subcategories" id="subcategories"></div>