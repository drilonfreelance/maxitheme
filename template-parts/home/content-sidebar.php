<div class="right-sidebar">
  <ul class="menu">
  <?php if(! is_user_logged_in() ): ?>
    <li class="parent" data-id="shopping-account">
      <a href="#"><span class="account-span">Kycu</span></a>
    </li>
<?php else: ?>
    <li class="parent" data-id="profile-info">
      <a href="#"
        ><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/menu/ikona-menu-account.svg" alt=""
      /></a>
    </li>
<?php endif; ?>
    <li class="parent" data-id="profile-order">
      <a href="#" class="menu-cart" onclick="maxi_get_cart_items_ajax()"
        ><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/menu/ikona-menu-shporta.svg" alt=""
      /></a>
    </li>
    <li class="parent" data-id="favourites-products">
      <a href="#"
        ><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/menu/ikona-menu-favourites.svg" alt=""
      /></a>
    </li>
    <li class="parent" data-id="waiting-order">
      <a href="#"
        ><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/menu/ikona-menu-historia.svg" alt=""
      /></a>
    </li>
    <li class="parent" data-id="settings-info">
      <a href="#"
        ><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/menu/ikona-menu-settings.svg" alt=""
      /></a>
    </li>
    <li class="parent bottom-menu" data-id="message-holder">
      <a href="#"
        ><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/menu/ikona-menu-chat.svg" alt=""
      /></a>
    </li>
  </ul>
</div>
