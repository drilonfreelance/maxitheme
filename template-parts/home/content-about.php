<?php 

$aboutPage = get_posts( array(
  'post_type' => 'page',
  'meta_key' => '_wp_page_template',
  'meta_value' => 'templates/about.php'
) )[0];

?>

<div class="bg-title">
  <div class="container">
    <h3>Rreth Maxi E-Shop</h3>
    <div class="line"></div>
  </div>
</div>
<div class="section-images">
  <div class="row m-t-3">
    <div class="col-md-6">
      <div class="item">
        <img src="<?php the_field('home_img', $aboutPage->ID); ?>" alt="" />
        <div class="overlay-wrapper">
          <div class="overlay">
            <div class="overlay-inner">
              <a href="<?php echo get_permalink($aboutPage); ?>">
                <div class="row">
                  <div class="col-md-4">
                    <h3><?php the_field('red_section_', $aboutPage->ID); ?></h3>
                  </div>
                  <div class="col-md-8">
                    <p>
                      <?php the_field('red_section_p', $aboutPage->ID); ?>
                    </p>
                    <div class="read-more">
                      Lexo Me shume <i class="fa fa-plus"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <img src="<?php echo get_the_post_thumbnail_url($aboutPage); ?>" alt="" />
    </div>
  </div>
  <div class="row m-t-2">
    <div class="col-md-6">
      <iframe
        src="https://www.youtube.com/embed/mRbNQ8Xl14A"
        frameborder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
        allowfullscreen
      ></iframe>
    </div>
    <div class="col-md-6">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/images/photos/about-maxi1.png" alt="" />
    </div>
  </div>
</div>
