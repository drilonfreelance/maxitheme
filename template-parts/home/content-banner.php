<div class="banner-images">
  <div class="row">
    <div class="col-md-8">
      <img src="<?php the_field('main_image', $post->ID); ?>" class="slider-img" alt="" />
    </div>
    <div class="col-md-4">
      <ul class="banner-images__inner">
        <li><img src="<?php the_field('small_image_1', $post->ID); ?>" alt="" /></li>
        <li><img src="<?php the_field('small_image_2', $post->ID); ?>" alt="" /></li>
        <li><img src="<?php the_field('small_image_3', $post->ID); ?>" alt="" /></li>
        <li><img src="<?php the_field('small_image_4', $post->ID); ?>" alt="" /></li>
      </ul>
    </div>
  </div>
</div>
