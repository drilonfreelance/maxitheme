<?php

foreach($items as $item => $values) { 
    $_product =  wc_get_product( $values['data']->get_id()); 
    $_product_image = wp_get_attachment_image_src( get_post_thumbnail_id( $_product->get_id() ) )[0];
    ?>

        <section id="item_<?php echo $values['data']->get_id(); ?>">
                <div class="my-order">
                    <div class="row">
                        <div class="col-md-3">
                            <img src="<?php echo $_product_image; ?>" alt="">
                        </div>
                        <div class="col-md-4">
                            <span class="product-name">
                                <a href="<?php echo get_permalink($_product->get_id()); ?>"> <?php echo $_product->get_title(); ?></a>
                            </span>
                            <div class="product-price m-t-2">
                                <div class="flag-origin">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/albanian-flag.png" alt="" />
                                </div>
                                <div class="price-info">
                                    <span class="span-red"><?php echo get_woocommerce_currency_symbol().$_product->get_sale_price(); ?></span>
                                    <span class="small-span"><?php echo get_woocommerce_currency_symbol().$_product->get_regular_price(); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="right">
                                <ul class="btn-list">
                                    <li>
                                    <a onclick="remove_from_cart('<?php echo $item; ?>', 'item_<?php echo $values['data']->get_id(); ?>')"><i class="fa fa-times"></i></a>
                                   </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

    <?php
}

?>