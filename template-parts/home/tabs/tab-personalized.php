<div id="settings-info">
            <div class="sidebar-content">
                <div class="products">
                    <h5>Pervoja e personalizuar</h5>
                    <div class="red-border-bottom"></div>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora, ullam fugiat! Libero possimus
                        ad quibusdam. Deleniti delectus distinctio, iusto vel facilis suscipit assumenda quam natus
                        voluptatum in cum itaque necessitatibus, tenetur eveniet ad animi! Omnis dolorum reprehenderit
                        voluptates distinctio quis?
                    </p>
                    <div class="m-t-5">
                        <h5>Email marketingu</h5>
                        <div class="red-border-bottom"></div>
                        <div class="row">
                            <div class="col-md-10">
                                <p class="border-p">Ne dergojme email marketingu ne dizarium@</p>
                            </div>
                            <div class="col-md-2">
                                <button class="edit-btn"><i class="fa fa-edit"></i></button>
                            </div>
                        </div>
                        <div class="m-t-3">
                            <div class="row">
                                <div class="col-md-10">
                                    <ul>
                                        <li>
                                            <p>Lajme rreth rikthimeve, artikujve te rinj dhe me shume</p>
                                        </li>
                                        <li>
                                            <p>Kerkesat per te kerkuar artikujt ne zbritje</p>
                                        </li>
                                        <li>
                                            <p>Ftese per te plotesuar kerkesat e konsumatoreve</p>
                                        </li>
                                        <li>
                                            <p>Zbritje per periudha te shkurtera</p>
                                        </li>
                                        <li>
                                            <p>Kampanja marketingu</p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-2">
                                    <ul>
                                        <li>
                                            <input type="checkbox" id="checkbox-1-1" class="regular-checkbox" /><label
                                                for="">
                                        </li>
                                        <li>
                                            <input type="checkbox" id="checkbox-1-1" class="regular-checkbox" /><label
                                                for="">
                                        </li>
                                        <li>
                                            <input type="checkbox" id="checkbox-1-1" class="regular-checkbox" /><label
                                                for="">
                                        </li>
                                        <li>
                                            <input type="checkbox" id="checkbox-1-1" class="regular-checkbox" /><label
                                                for="">
                                        </li>
                                        <li>
                                            <input type="checkbox" id="checkbox-1-1" class="regular-checkbox" /><label
                                                for="">
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="m-t-100">
                            <p class="red-p">Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora, ullam
                                fugiat! Libero possimus ad quibusdam.
                            </p>
                            <div class="red-border-bottom"></div>
                        </div>
                    </div>
                    <div class="bottom-bar">
                        <div class="row">
                            <div class="col-md-12 center">
                                <h5>Ruaj Ndryshimet</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>