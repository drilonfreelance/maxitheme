<?php 

$customer_orders = wc_get_orders( array(
    'meta_key' => '_customer_user',
    'meta_value' => get_current_user_id(),
    'post_status' => array('wc-on-hold', 'wc-processing', 'wc-completed'),
    'numberposts' => -1
) );

?>

<div id="waiting-order">
            <div class="sidebar-content">
                <div class="products">

                <?php 
                // $_product_image = wp_get_attachment_image_src( get_post_thumbnail_id( $item->get_product_id() ) )[0];
                foreach($customer_orders as $order ){
                    $count = 0;
                    ?>
                    <section class=" <?php echo($order->get_status()=='on-hold')? 'red-section':'' ?>">
                        <div class="my-order">
                            <h6><?php echo $order->get_status(); ?></h6>
                            <span>Data e Fatures <?php echo $order->get_date_created()->format ('Y-m-d'); ?></span>Numri i fatures <?php echo $order->get_id(); ?> </span>
                            <div class="row m-t-1">
                                <div class="col-md-3">
                                    <?php 
                                    foreach($order->get_items() as $item ){
                                        if($count == 0){
                                            $_product_image = wp_get_attachment_image_src( get_post_thumbnail_id( $item->get_product_id() ) )[0];
                                        ?>
                                            <img src="<?php echo $_product_image; ?>" alt="">
                                        <?php
                                        }
                                        break;
                                    }
                                    ?>
                                </div>
                                <div class="col-md-5">
                                    <span class="product-name">
                                    <?php 
                                    
                                    foreach($order->get_items() as $item ){
                                        echo $item->get_name() . ' , </br>';
                                    }

                                    ?>    
                                    </span>
                                </div>
                                <div class="col-md-4">
                                    <div class="right">
                                        <h6>Totali <?php echo $order->get_total(); ?></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <?php
                }
                ?>
                </div>
            </div>
        </div>