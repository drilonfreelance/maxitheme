<div id="message-holder">
            <div class="sidebar-content">
                <div class="products">
                    <div class="title">
                        <div class="row">
                            <div class="col-md-2">
                                <i class="fa fa-comments"></i>
                            </div>
                            <div class="col-md-8">
                                <h5>Si mund te ju ndihmojme ?</h5>
                            </div>
                            <div class="col-md-2">
                                <div class="active-status"></div>
                            </div>
                        </div>
                    </div>
                    <div class="message-content">
                        <div class="container">
                            <div class="Area">
                                <div class="R">
                                    <img src="assets/images/icons/avatar.png" />
                                    <div class="tooltip">Pershendetje</div>
                                </div>
                                <div class="text R textR">Pershendetje
                                </div>
                                <div class="text R textR">test
                                </div>
                                <div class="text R textR">test
                                </div>
                                <div class="text R textR">Kam nevoj per pak ndihme, po provoj me u regjistru po spo
                                    muna, a
                                    muneni ju lutna ndonje numer te tel apo diqka te ju kontaktoj per tu informuar me
                                    mir ?
                                </div>
                            </div>
                            <div class="Area automatic-msg">
                                <div class="L">
                                    <img src="assets/images/icons/logo-maxi-msg.png" />
                                    <div class="tooltip">Pershendetje</div>
                                </div>
                                <div class="text L textL"><span class="bold-span">Mesazh Automatik</span>
                                    <br> Klient i nderuar, ne kemi pranuar mesazhin tuaj dhe shumë shpejt do të kthehemi
                                    tek
                                    ju me përgjigjie. Ju mund të kontaktoni me stafin tonë në kohë reale nga ora 7:00
                                    deri
                                    në orën 22:00. Gjithë të mirat!
                                </div>
                            </div>
                            <div class="Area">
                                <div class="R">
                                    <img src="assets/images/icons/avatar.png" />
                                    <div class="tooltip">Pershendetje</div>
                                </div>
                                <div class="text L textL">Okay
                                </div>
                            </div>
                            <div class="Area left-sent-msg">
                                <div class="L">
                                    <img src="assets/images/icons/logo-maxi-msg.png" />
                                    <div class="tooltip">Pershendetje</div>
                                </div>
                                <div class="text L textL">
                                    Pershendetje Arta,
                                    <br> Jam Kaltrina nga qendra e informacionit ne eMaxi, Si munde tju ndihmoje ?
                                    Gjithë të
                                    mirat!
                                </div>
                            </div>
                        </div>
                    </div>
                    <textarea class="" placeholder="Mesazhi Juaj"></textarea>
                </div>
            </div>
        </div>