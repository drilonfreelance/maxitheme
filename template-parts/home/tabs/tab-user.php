<div id="profile-info">
            <div class="sidebar-content">
                <div class="products">
                    <h5>Te dhenat gjenerale</h5>
                    <div class="red-border-bottom"></div>

                    <div class="m-t-3">
                        <a class="sidebar-btn" href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>/edit-account">
                            Edit account
                        </a>
                    </div>
                    <div class="m-t-3">
                        <a class="sidebar-btn" href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>/edit-address/billing/">
                            Edit billing
                        </a>
                    </div>
                    <div class="m-t-3">
                        <a class="sidebar-btn" href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>/edit-address/shipping/">
                            Edit shiping
                        </a>
                    </div>
                </div>
            </div>
        </div>