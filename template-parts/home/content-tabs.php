<div class="collapsed-tabs">
<?php 

$tab = 'template-parts/home/tabs/tab';

get_template_part($tab, 'cart'); 
get_template_part($tab, 'creds'); 
get_template_part($tab, 'orders'); 
get_template_part($tab, 'personalized'); 
get_template_part($tab, 'user'); 
get_template_part($tab, 'chat'); 
get_template_part($tab, 'wish'); 

?>
</div>