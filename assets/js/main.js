$(document).ready(function($) {
  // Slider change of images
  function sliderImages() {
    var $mainSlide = $("img.slider-img");

    $(".banner-images .banner-images__inner li").on("click", function() {
      var $newSource = $(this)
        .find("img")
        .attr("src");

      $mainSlide.attr("src", $newSource);
    });
  }

  function sidebarMenu() {
    // Menu Cart
    $(".right-sidebar ul li").on("click", function() {
      // $('#notification-settings').toggleClass('visible-box');
      var theID = $(this).data("id");
      $(this).toggleClass("active");

      $(".collapsed-tabs > div").each(function() {
        if ($(this).attr("id") === theID) {
          $(this).addClass("visible-box");
        } else {
          $(this).removeClass("visible-box");
        }
      });
      $("header, .main-wrapper .row .col-md-11").on("click", function(e) {
        $(".collapsed-tabs > div").each(function() {
          $(this).removeClass("visible-box");
        });
      });
    });
  }

  function init() {
    sliderImages();
    sidebarMenu();
  }

  // Call all the functions within int
  init();
});
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

$(function() {
  $(".tab-link").on("click", function() {
    $(".tab-link").removeClass("active");
    $(".tabcontent").hide();
    $("#" + $(this).data("tab")).show();
    $(this).addClass("active");
  });
});

function maxi_more(parentelement) {
  let total =
    parseInt(
      parentelement.getElementsByClassName("max_qty_total")[0].innerText
    ) + 1;
  parentelement.getElementsByClassName("max_qty_total")[0].innerText = total;
}

function maxi_less(parentelement) {
  let total =
    parseInt(
      parentelement.getElementsByClassName("max_qty_total")[0].innerText
    ) - 1;
  if (total > 0) {
    parentelement.getElementsByClassName("max_qty_total")[0].innerText = total;
  }
}
function load_sub_categories(item, current) {
  jQuery(document).ready(function($) {
    $.ajax({
      url: ajaxurl,
      type: "POST",
      data: {
        action: "load_sub_categories",
        item: item
      },
      success: function(response) {
        show_sub_categories(response, item);
        highlight_active(current.parentElement, current);
      }
    });
  });
}

function show_sub_categories(json, parentId) {
  let items = JSON.parse(json);
  let maindiv = document.createElement("div");
  maindiv.classList.add("categories");
  let ul = document.createElement("ul");
  ul.classList.add("nav", "nav-tabs");
  let li = document.createElement("li");
  li.classList.add("nav-item", "test");
  let a = document.createElement("a");
  a.classList.add("nav-link");
  // let attr = "get_products('" + parentId + "', this.parentElement)";
  a.setAttribute("href", items[0].parent_link);
  a.innerText = "All products";
  li.appendChild(a);
  ul.appendChild(li);
  for (var i = 0; i < items.length; i++) {
    let li = document.createElement("li");
    li.classList.add("nav-item");
    let a = document.createElement("a");
    a.classList.add("nav-link");
    a.innerText = items[i].term.name;
    a.setAttribute("href", items[i].link);
    let img = document.createElement("img");
    img.setAttribute("src", items[i].image);
    a.innerHTML = "<span>" + items[i].term.name + "</span>";
    a.appendChild(img);
    li.appendChild(a);
    ul.appendChild(li);
  }
  maindiv.appendChild(ul);
  document.getElementById("subcategories").innerHTML = "";
  document.getElementById("subcategories").appendChild(maindiv);
  // get_products(parentId, li);
}

function get_products(item, current) {
  jQuery(document).ready(function($) {
    $.ajax({
      url: ajaxurl,
      type: "POST",
      data: {
        action: "get_products_maxi",
        item: item
      },
      success: function(response) {
        generate_products(response);
        highlight_active(current.parentElement, current);
        change_wish_icon();
      }
    });
  });
}

function change_wish_icon() {
  jQuery(".add_to_wishlist").each(function(index) {
    jQuery(this).html(`
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">
      <path d="M24.85,10.126c2.018-4.783,6.628-8.125,11.99-8.125c7.223,0,12.425,6.179,13.079,13.543
        c0,0,0.353,1.828-0.424,5.119c-1.058,4.482-3.545,8.464-6.898,11.503L24.85,48L7.402,32.165c-3.353-3.038-5.84-7.021-6.898-11.503
        c-0.777-3.291-0.424-5.119-0.424-5.119C0.734,8.179,5.936,2,13.159,2C18.522,2,22.832,5.343,24.85,10.126z"/>
      </svg>
    `);
  });
}

function highlight_active(item, child) {
  for (var i = 0; i < item.children.length; i++) {
    if (item.children[i].classList.contains("active"))
      item.children[i].classList.remove("active");
  }
  child.classList.add("active");
}

function generate_products(json_products) {
  document.getElementById("products").innerHTML = json_products;
}

function remove_from_cart(item, current) {
  jQuery(document).ready(function($) {
    $.ajax({
      url: ajaxurl,
      type: "POST",
      data: {
        action: "remove_item",
        item: item
      },
      success: function(response) {
        console.log(current);
        document.getElementById(current).remove();
      }
    });
  });
}
function maxi_get_cart_items_ajax() {
  jQuery(document).ready(function($) {
    $.ajax({
      url: ajaxurl,
      type: "POST",
      data: {
        action: "maxi_get_cart_items_ajax"
      },
      success: function(response) {
        document.getElementById("cart-products").innerHTML = response;
      }
    });
  });
}
jQuery(".heart-cart").each(function(index) {
  jQuery(".ajax-loading").remove();
});
