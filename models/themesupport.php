<?php
class Themesupport{

    public static function register_nav_menu( $location, $menu_name ){
        register_nav_menus( array(
            $location => $menu_name,
        ) );
    }

    public static function add_theme_support( $name ){
        add_theme_support( $name );
    }

    public static function add_active_nav(){
        add_filter('nav_menu_css_class' , array( new self(), 'special_nav_class' ) , 10 , 2 );
    }

    public function special_nav_class ( $classes, $item ) {
        if ( in_array( 'current-menu-item', $classes ) ){
            $classes[] = 'active ';
        }
        return $classes;
    }

}