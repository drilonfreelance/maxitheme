<?php
class AdminScript{

    public static function hide_menu( $menu_name ) {
        add_action( 'admin_menu', function() use ( $menu_name ) {
            remove_menu_page( $menu_name );
        } ,999 );
    }

    public static function hide_submenu( $menu_name, $submenu_name ) {
        add_action( 'admin_menu', function() use ( $menu_name, $submenu_name ) {
            remove_menu_page( $menu_name );
            remove_submenu_page( $menu_name, $submenu_name );
        } ,999 );
    }

    public function dswp_remove_site_name( $wp_admin_bar ) {
        $wp_admin_bar->remove_node( 'wp-logo' );
    }

    public static function load_admin_style() {

        add_action( 'login_enqueue_scripts', function() {
            wp_register_style( 'custom_wp_login_css', get_template_directory_uri() . '/assets/admin/login-style.css', false, '1.0.0' );
            wp_enqueue_style( 'custom_wp_login_css' );
        } );

        add_action( 'admin_bar_menu', array( new self, 'dswp_remove_site_name' ),999 );

        add_action( 'admin_bar_menu', function() {
            wp_register_style( 'custom_wp_admin_menu_css', get_template_directory_uri() . '/assets/admin/admin-bar.css', false, '1.0.0' );
            wp_enqueue_style( 'custom_wp_admin_menu_css' );
        },999 );
    }    

}