<?php

class Script{

    public static function init_built_in_libraries() {
        add_action( 'wp_enqueue_scripts', function() {
            wp_enqueue_style( 'bootstrap-css', get_template_directory_uri().'/assets/css/bootstrap.min.css', array(), '2.0.0', false );
            wp_enqueue_style( 'font-awesome-css', get_template_directory_uri().'/assets/css/font-awesome.css', array(), '3.0.0', false );
            wp_enqueue_style( 'owl-carousel-css', get_template_directory_uri().'/assets/css/owl.carousel.css', array(), '3.0.0', false );
            wp_enqueue_style( 'main-style-css', get_template_directory_uri().'/assets/css/style.css', array(), '3.0.0', false );
            wp_enqueue_script( 'jquery-js', get_template_directory_uri().'/assets/js/jquery-2.1.4.js', array(), '1.0.0', true );
            wp_enqueue_script( 'bootstrap-js', get_template_directory_uri().'/assets/js/bootstrap.min.js', array(), '1.0.0', true );
            wp_enqueue_script( 'owl-carousel-js', get_template_directory_uri().'/assets/js/owl.carousel.js', array(), '1.0.0', true );
            wp_enqueue_script( 'owl-carousel2-thumbs-js', get_template_directory_uri().'/assets/js/owl.carousel2.thumbs.js', array(), '1.0.0', true );
            wp_enqueue_script( 'main-js', get_template_directory_uri().'/assets/js/main.js', array(), '1.0.0', true );
        } );
    }

    public static function init_custom_script( $handler, $path, $in_footer = 'all' ) {
        add_action( 'wp_enqueue_scripts',function() use ( $handler, $path, $in_footer ) {
            wp_enqueue_script( $handler, get_template_directory_uri() . $path, array(), '1.0.0', $in_footer );
        });
    }

    public static function init_custom_script_with_args( $handler, $path, $object_name, $data ) {

        add_action( 'wp_enqueue_scripts', function() use ( $handler, $path, $object_name, $data ) {
            wp_register_script( $handler, get_template_directory_uri() . $path );
            wp_localize_script( $handler, $object_name, $data );
            wp_enqueue_script( $handler );
        } );

    }

    public static function insert_script_on_custom_page( $template, $handler, $path, $in_footer = 'all', $object_name = false, $data = false) {
        add_action( 'template_redirect', function() use ( $template, $handler, $path, $in_footer, $object_name, $data ) {
            if( is_page_template( $template ) ) :
                if ( $data != false && $object_name != false ) :
                    self::init_custom_script_with_args( $handler, $path, $object_name, $data );
                else :
                    self::init_custom_script( $handler, $path, $in_footer );
                endif;
            endif;
        } );
    }

}