<?php 
class Customwidget{

    public static function register( $widgetName, $widgetId ){
        register_sidebar( array(
            'name'  => __( $widgetName, 'starter-theme'),
            'id'    => $widgetId
        ) );
    }
    
}