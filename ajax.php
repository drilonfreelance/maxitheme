<?php

function load_sub_categories(){
    $cat = sanitize_text_field($_POST['item']);

    $cat_ids = get_term_children($cat, 'product_cat');

    $categories = [];

    foreach($cat_ids as $id):
        $image = wp_get_attachment_url( get_term_meta( $id, 'thumbnail_id', true ) );
        array_push($categories, array(
            'term' => get_term($id, 'product_cat'),
            'image' => $image,
            'link' => get_term_link($id, 'product_cat'),
            'parent_link' => get_term_link( get_term($id, 'product_cat')->parent, 'product_cat' )
        ));
    endforeach;

    wp_send_json(json_encode($categories));
 }
add_action( 'wp_ajax_load_sub_categories', 'load_sub_categories' );
add_action( "wp_ajax_nopriv_load_sub_categories", "load_sub_categories" );

function get_products_maxi(){
    $cat = sanitize_text_field($_POST['item']);
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => '-1',
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'term_id',
                'terms' => $cat
            )
        )
    );

    $query = new WP_Query($args);

    if($query->have_posts()):
        while($query->have_posts()): $query->the_post();
            wc_get_template_part( 'woocommerce/content', 'product' );
        endwhile;
    endif;
    wp_die();
 }
add_action( 'wp_ajax_get_products_maxi', 'get_products_maxi' );
add_action( "wp_ajax_nopriv_get_products_maxi", "get_products_maxi" );

function remove_item(){
    $item_id = sanitize_text_field($_POST['item']);
    WC()->cart->remove_cart_item($item_id);

    wp_send_json('0');
 }
add_action( 'wp_ajax_remove_item', 'remove_item' );
add_action( "wp_ajax_nopriv_remove_item", "remove_item" );

function maxi_get_cart_items_ajax(){
    global $woocommerce;
    $items = $woocommerce->cart->get_cart();

    set_query_var('items', $items);
    get_template_part('template-parts/home/tabs/tab', 'cart-item');

    die();
 }
add_action( 'wp_ajax_maxi_get_cart_items_ajax', 'maxi_get_cart_items_ajax' );
add_action( "wp_ajax_nopriv_maxi_get_cart_items_ajax", "maxi_get_cart_items_ajax" );